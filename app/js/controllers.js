'use strict';

var riskControllers = angular.module('riskControllers', ["chart.js"]);

// Handles everything relating to the quote page
riskControllers.controller('QuoteController', ['$scope', '$log', '$http', '$templateCache', '$q', 'UserFactory', 'OrderFactory', '$modal', 'user',
   function($scope, $log, $http, $templateCache, $q, UserFactory, OrderFactory, $modal, user) {
   $scope.loggedIn = user.success;
   $scope.pageElements = [];

   // Get the stock codes and attach it to the codes dict
   $http.get('app/static/codes200A.json').then(function(response) {
      $scope.codes = response.data;
      $scope.model = {typedStock: $scope.codes[0].name};
      $scope.stock = $scope.codes[0];
      $scope.fetch();
   });

   $scope.$watch('model.typedStock',function(){})

   $scope.togglePageElms = function(pageElement) {
      var index = $scope.pageElements.indexOf(pageElement);
      if (index == -1) {
         $scope.pageElements.push(pageElement);
      } else {
         $scope.pageElements.splice(index, 1);
      }
   }

   // Gets the current information for the stock (includes price and the depth)
   $scope.fetchCurrentInfo = function() {
      $scope.processing = "Processing...";
      $scope.price = null;
      $scope.url = "https://query.yahooapis.com/v1/public/yql?q=" +
                     "select%20*%20from%20yahoo.finance.quotes%20where%20symbol%3D%22" +
                     $scope.stock.code +
                     "%22&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
      var deferred = $q.defer();

      // Perform query to get current info on selected stock
      $http({method: 'GET', url: $scope.url, cache: $templateCache}).then(function(response) {
         $scope.yahooStatus = response.status;
         $scope.data = response.data;
         if ($scope.data != null &&
            $scope.data.query != null &&
            $scope.data.query.results != null &&
            $scope.data.query.results.quote != null) {

            $scope.quote = $scope.data.query.results.quote;
            $scope.price = parseFloat($scope.quote.LastTradePriceOnly);

            // Fetch market depth
            var parameter = JSON.stringify({'stock': $scope.stock.code, 'max_bid': parseFloat($scope.quote.Bid), 'min_ask': parseFloat($scope.quote.Ask), 'avg_volume': parseFloat($scope.quote.AverageDailyVolume)});
            $http.post('/depth', parameter).success(function(response) {
               $scope.depth = response;
               $scope.depthBids = $scope.$eval($scope.depth.bids);
               $scope.depthAsks = $scope.$eval($scope.depth.asks);

               //Get a list of depth bids in descending order
               $scope.depthKeys = [];
               for(var key in $scope.depthBids) {
                  if($scope.depthBids.hasOwnProperty(key)) { //to be safe
                     $scope.depthKeys.push(key);
                  }
               }
               $scope.depthKeys.sort(function(a, b){return b-a});

               deferred.resolve();
            }, function(response) {
               $scope.depthStatus = "Cannot get market depth. Please ensure you have selected a stock and try again"
               deferred.reject();
            });
         }
         $scope.processing = "";
      }, function(response) {
         $scope.data = response.data || "Request failed";
         $scope.yahooStatus = response.status;
         $scope.processing = "";
         deferred.reject();
      });
      return deferred.promise;
   }

   $scope.fetchDividends = function() {
      $scope.getCurrentDate();
      $scope.d1 = "1960-01-01";
      var divUrl = "https://query.yahooapis.com/v1/public/yql?q=" +
      "select%20*%20from%20yahoo.finance.dividendhistory%20where%20symbol%20%3D%20%22" +
      $scope.stock.code + "%22%20and%20startDate%20%3D%20%22"+
      $scope.d1 +"%22%20and%20endDate%20%3D%20%22"+
      $scope.d2 +"%22&diagnostics=false&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

      $scope.dividends = [];
      $http({method: 'GET', url: divUrl, cache: $templateCache})
      .then(function(response) {
         if (response.data.query.count == 0) {
            $scope.dividends.LastDate = "None found!"
            $scope.dividends.LastAmount = "None found!"
         } else {
            $scope.dividends.responseStatus = response.status;
            $scope.dividends.responseData = response.data.query.results.quote;
            // Most recent dividend always indexed at 0
            $scope.dividends.LastDate = $scope.dividends.responseData[0].Date;
            $scope.dividends.LastAmount = $scope.dividends.responseData[0].Dividends;
         }
      }, function(response) {
         // nothing, errors handled above
      });
   }

   // Fetch the news stories for the selected stock
   $scope.fetchNews = function() {
      var url = "//query.yahooapis.com/v1/public/yql?q=select%20*%20from" +
      "%20html%20where%20url%3D'https%3A%2F%2Fau.finance.yahoo.com%2Fq%2Fp%3Fs%3D" +
      $scope.stock.code + "'%20and%20xpath%3D'%2F%2Ful%2Fli%5Bcite%5D'&" +
      "format=json&diagnostics=true";

      $http.get(url)
      .success(function(response) {
         if (response.query.results) {
            // If only one story is returned, some extra processing is required.
            if (response.query.results.li.a) {
               var s = [];
               s.push(response.query.results.li)
               $scope.stories = s;
            } else {
               $scope.stories = response.query.results.li;
            }
         } else {
            $scope.stories = null;
         }
      })
      .error(function(response) {
         $scope.stories = null;
      });
   }

   // Create the historical graph of the selected stock
   $scope.fetchHistoricalPrices = function() {

      $scope.getCurrentDate();

      // URL to get historical prices of selected stock
      $scope.historicalURL = "https://query.yahooapis.com/v1/public/yql?q=" +
         "select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22" +
         $scope.stock.code + "%22%20and%20startDate%20%3D%20%22" +
         $scope.d1 + "%22%20and%20endDate%20%3D%20%22" +
         $scope.d2 + "%22&format=json&diagnostics=false&" +
         "env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";


      $scope.chartConfig = {
            options: {
               chart: {
                  zoomType: 'x'
               },
               rangeSelector: {
                  // selected: 1,
                  enabled: true
               },
               navigator: {
                  enabled: true,
                  series: {
                     data: $scope.chartPrices
                  }
               }
            },
            title: {
               text:'',
               style: {
                  display: 'none'
               }
            },

            useHighStocks: true
         }

      // Perform query to get historical prices of selected stock
      $http({method: 'GET', url: $scope.historicalURL, cache: $templateCache})
      .then(function(response) {
         $scope.historicalStatus = response.status;
         $scope.historicalDataQuote = response.data.query.results.quote;

         // Create data for graphs
         $scope.chartPrices = [];
         $scope.chartVolumes = [];

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth(); //January is 0!
         var yyyy = today.getFullYear();

         // Maybe += 5
         for (var i = $scope.historicalDataQuote.length - 1; i >= 0; i -= 1) {
            var quoteDate = $scope.historicalDataQuote[i].Date;

            var yyyy = quoteDate.substring(0,4);
            var mm   = quoteDate.substring(5,7)-1;
            var dd   = quoteDate.substring(8,10);

            var currentList = $scope.chartPrices;
            var newList = currentList.concat([[Date.UTC(yyyy, mm, dd), parseFloat($scope.historicalDataQuote[i].Close)]]);
            $scope.chartPrices = newList;

            currentList = $scope.chartVolumes;
            newList = currentList.concat([[Date.UTC(yyyy, mm, dd), parseFloat($scope.historicalDataQuote[i].Volume)]]);
            $scope.chartVolumes = newList;
         }

         $scope.chartConfig = {
            options: {
               chart: {
                  zoomType: 'x'
               },
               rangeSelector: {
                  // selected: 1,
                  enabled: true
               },
               navigator: {
                  enabled: true,
                  series: {
                     data: $scope.chartPrices
                  }
               }
            },
            title: {
               text:'',
               style: {
                  display: 'none'
               }
            },

            xAxis: [{
               type: 'datetime',
               lineWidth: 1,
               tickWidth: 2,
               endOnTick: false,
               startOnTick: false,
               ordinal: false,
            }],

            yAxis: [{
                labels: {
                    align: 'right',
                    x: -3
                },
                title: {
                    text: 'Price'
                },
                height: '60%',
                lineWidth: 2
            }, {
                labels: {
                    align: 'right',
                    x: -3
                },
                title: {
                    text: 'Volume'
                },
                top: '65%',
                height: '35%',
                offset: 0,
                lineWidth: 2
            }],

            series: [{
               // id: 1,
               name: 'Equity price',
               data: $scope.chartPrices
            }, {
               // id: 2,
               type: 'column',
               yAxis: 1,
               name: 'Volume',
               data: $scope.chartVolumes
            }],

            useHighStocks: true
         }

         $scope.processingHistory = "";

      }, function(response) {
        //do nothing
      });
      $scope.processingHistory = "Processing...";

   }

   $scope.addAsxOverlay = function(overlayASX) {
      if (!overlayASX) {
         // Remove ASX overlay
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "ASX") {
               $scope.chartConfig.series[i].visible = false;
               return true;
            }
         }
      }
      if (typeof $scope.asxPrices != "undefined" && $scope.asxPrices[0][1] == $scope.chartPrices[0][1]) {
         // Avoid recalculating if ASX data already defined
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "ASX") {
               $scope.chartConfig.series[i].visible = true;
               return true;
            }
         }
      } else {
         // Calculate normalised ASX data
         $scope.processingHistory = "Processing...";
         $scope.getCurrentDate();

         $scope.asxURL = "https://query.yahooapis.com/v1/public/yql?q=" +
            "select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22" +
            "ASX.AX%22%20and%20startDate%20%3D%20%22" +
            $scope.d1 + "%22%20and%20endDate%20%3D%20%22" +
            $scope.d2 + "%22&format=json&diagnostics=false&" +
            "env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

         // grab ASX historical data over same timeframe for ASX overlay
         $http({method: 'GET', url: $scope.asxURL, cache: $templateCache})
         .then(function(response) {
            $scope.asxStatus = response.status;
            if ($scope.asxStatus != 200) {
               $scope.processingHistory = "Something went wrong with the Yahoo api query";
            }
            $scope.asxDataQuote = response.data.query.results.quote;

            $scope.asxPrices = [];
            // Maybe += 5
            for (var i = $scope.asxDataQuote.length - 1; i >= 0; i -= 1) {
               var quoteDate = $scope.asxDataQuote[i].Date;

               var yyyy = quoteDate.substring(0,4);
               var mm   = quoteDate.substring(5,7)-1;
               var dd   = quoteDate.substring(8,10);

               var currentList = $scope.asxPrices;
               var newList = currentList.concat([[Date.UTC(yyyy, mm, dd), parseFloat($scope.asxDataQuote[i].Close)]]);
               $scope.asxPrices = newList;
            }


            // normalise ASX to current stock
            var denom = $scope.asxPrices[0][1]/$scope.chartPrices[0][1];
            for (var j = 0; j < $scope.asxPrices.length; j++) {
               $scope.asxPrices[j][1] = $scope.asxPrices[j][1]/denom;
            }

            $scope.chartConfig.series.push({
               name: "ASX",
               data: $scope.asxPrices
            });
            $scope.processingHistory = "";
         }, function(response) {
            //nada
         });
      }
   }

   $scope.addSmaOverlay = function(overlaySMA) {
      if (!overlaySMA) {
         // Remove SMA overlay
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "SMA") {
               $scope.chartConfig.series[i].visible = false;
               return true;
            }
         }
      }
      if (typeof $scope.smaPrices != "undefined" && $scope.smaPrices[0][1] == $scope.chartPrices[0][1]) {
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "SMA") {
               $scope.chartConfig.series[i].visible = true;
               return true;
            }
         }
         // If we haven't found SMA in the chartConfig yet, it's because it's been calculated by
         // addEmaOverlay but not added. Adding here:
         $scope.chartConfig.series.push({
            name: "SMA",
            data: $scope.smaPrices
         });
      } else {
         var bIncrement = 10;
         $scope.smaPrices = calculateSMA(bIncrement);
         $scope.chartConfig.series.push({
            name: "SMA",
            data: $scope.smaPrices
         });
      }
   }

   $scope.addEmaOverlay = function(overlayEMA) {
      if (!overlayEMA) {
         // Remove EMA overlay
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "EMA") {
               $scope.chartConfig.series[i].visible = false;
               return true;
            }
         }
      }
      if (typeof $scope.emaPrices != "undefined" && $scope.emaPrices[0][1] == $scope.chartPrices[0][1]) {
         for (var i = 0; i < $scope.chartConfig.series.length; i++) {
            if ($scope.chartConfig.series[i].name == "EMA") {
               $scope.chartConfig.series[i].visible = true;
               return true;
            }
         }
      } else {
         var bIncrement = 10;
         if (typeof $scope.smaPrices == "undefined" || $scope.smaPrices[0][1] != $scope.chartPrices[0][1]) {
            // EMA uses SMA prices, so if SMA undefined or wrong, recalculate it.
            $scope.smaPrices = calculateSMA(bIncrement);
         }
         var multiplier = 2/(bIncrement + 1);
         // Start EMA with first element of SMA, build iteratively on this
         $scope.emaPrices = [$scope.smaPrices[0]];
         for (var j = 1; j < $scope.smaPrices.length; j++) {
            var histIndex = $scope.historicalDataQuote.length - 1 - j;
            // From http://stockcharts.com/school/doku.php?id=chart_school%3Atechnical_indicators%3Amoving_averages:
            // EMA: {Close - EMA(previous day)} x multiplier + EMA(previous day)
            var currentEma =
               ($scope.historicalDataQuote[histIndex].Close - $scope.emaPrices[j-1][1])*multiplier + $scope.emaPrices[j-1][1];
            var currentList = $scope.emaPrices;
            var newList = currentList.concat([[$scope.smaPrices[j][0], currentEma]]);
            $scope.emaPrices = newList;
         }

         $scope.chartConfig.series.push({
            name: "EMA",
            data: $scope.emaPrices
         });
      }
   }

   function calculateSMA(bIncrement) {
      var prices = [];
      for (var i = $scope.historicalDataQuote.length - 1; i >= 0; i -= 1) {
         var quoteDate = $scope.historicalDataQuote[i].Date;

         var yyyy = quoteDate.substring(0,4);
         var mm   = quoteDate.substring(5,7)-1;
         var dd   = quoteDate.substring(8,10);

         // Set increment to min(baseInc, length-1-i)
         var inc = $scope.historicalDataQuote.length - 1 - i;
         if (inc > bIncrement) {
            inc = bIncrement;
         }
         // Calculate average over last {inc} days
         var avg = 0;
         for (var j = 0; j < inc && j <= i; j++) {
            avg += parseFloat($scope.historicalDataQuote[i-j].Close)
         }
         if (j == 0) {
            avg = $scope.historicalDataQuote[i].Close;
            j = 1;
         }
         avg = avg/j;

         var currentList = prices;
         var newList = currentList.concat([[Date.UTC(yyyy, mm, dd), avg]]);
         prices = newList;
      }
      return prices;
   }

   // Initialises the current date (d1, d2)
   $scope.getCurrentDate = function() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10) dd='0'+dd;
      if(mm<10) mm='0'+mm;
      $scope.d1 = yyyy - 1 + "-" + mm + "-" + dd; // "2014-01-01";
      $scope.d2 = yyyy + "-" + mm + "-" + dd;     // "2014-12-31";
   }

   // Handles the selection of a stock code
   $scope.onSelect = function($item, $model, $label) {
      $scope.stock = $item;
      $scope.fetch();
   };

   $scope.fetch = function() {
      // Initialise call variables
      $scope.response = null;
      $scope.data = null;
      $scope.quote = null;
      $scope.graph_scale = "year";

      // Fetch the relevant information
      $scope.fetchCurrentInfo();
      $scope.fetchNews();
      $scope.fetchHistoricalPrices();
      $scope.fetchDividends();
   };

   // Modal to handle displaying company list
   $scope.openCompanyList = function (error) {
         $scope.selection = "";
         var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/app/partials/companies.html',
            controller: 'CompanyController',
            scope: $scope
         });

         modalInstance.result.then(function (selectedCompany) {
            $scope.stock = selectedCompany;
            $scope.model.typedStock = selectedCompany.name;
            $scope.fetch();
         });
   };

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.dismiss();
   };

   $scope.openOrderForm = function (error) {
      if ($scope.model.typedStock == $scope.stock.name) {
         $scope.status = "";
         var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/app/partials/orderform.html',
            controller: 'FormController',
            scope: $scope
         });

         modalInstance.result.then(function (status) {
            $scope.status = status;
         }, function (status) {
            if (status != 'escape key press' && status != 'backdrop click') {
               $scope.status = status;
            }
         });
      } else {
         $scope.status = "Please select a stock";
      }
   };


}]);


riskControllers.controller('CompanyController', ['$scope', '$modalInstance',
   function($scope, $modalInstance) {
   // Modal to handle displaying company list
   $scope.selectCompany = function(company) {
      $modalInstance.close(company);
   }

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.dismiss();
   };
}]);



// Handles everything related to the order form
riskControllers.controller('FormController', ['$scope', '$log', '$http', '$templateCache', '$q', 'UserFactory', 'OrderFactory', '$modal', '$modalInstance',
   function($scope, $log, $http, $templateCache, $q, UserFactory, OrderFactory, $modal, $modalInstance) {

   // Initially the form has not been submitted
   $scope.submitted = false;

   // There is no order status
   $scope.status = "";

   // Initialise order
   $scope.order = {};
   $scope.order.fee = 20.00;
   $scope.order.type = "buy"
   $scope.order.subtype = "market"
   $scope.order.stock = $scope.stock.code;
   $scope.order.price = $scope.price;


   $scope.updatePriceDisplay = function() {
      $scope.status = "";
      if ($scope.quote == null) {
         $scope.fetchCurrentInfo().then(function(resolve){
            $scope.updateMarketPrice();
         });
      } else {
         $scope.updateMarketPrice();
      }
   }


   $scope.updateMarketPrice = function() {
      if ($scope.order.subtype == 'market') {
         if ($scope.order.type == 'buy') {
            $scope.order.price = parseFloat($scope.quote.Ask);
         } else if ($scope.order.type == 'sell') {
            $scope.order.price = parseFloat($scope.quote.Bid);
         }
      }
   }

   // Modal to handle confirmation screen
   $scope.openOrderConfirm = function (error) {
      $scope.submitted = true;
      // Formatting validation
      if ($scope.order.subtype == "limit"
         && ($scope.order.price > $scope.quote.Ask && $scope.order.type == "buy")) {
         $scope.status = "A limit buy order must have an order price less than or equal to the Ask";
      } else if ($scope.order.subtype == "limit"
         && ($scope.order.price < $scope.quote.Bid && $scope.order.type == "sell")) {
         $scope.status = "A limit sell order must have an order price greater than or equal to the Bid";
      } else if ($scope.order.subtype == "stop"
         && ($scope.order.price < $scope.quote.Ask && $scope.order.type == "buy")) {
         $scope.status = "A stop buy order must have an order price greater than or equal to the Ask";
      } else if ($scope.order.subtype == "stop"
         && ($scope.order.price > $scope.quote.Bid && $scope.order.type == "sell")) {
         $scope.status = "A stop sell order must have an order price less than or equal to the Bid";
      } else if (($scope.order.type == "buy" ||  $scope.order.type == "sell")
         && ($scope.order.subtype == "market" || $scope.order.subtype == "limit" || $scope.order.subtype == "stop")
         && $scope.order.stock != null
         && $scope.order.price >= 0
         && $scope.order.quantity > 0) {

         var modalInstance = $modal.open({
            animation: true,
            templateUrl: '/app/partials/confirmation.html',
            controller: 'OrderController',
            scope: $scope
         });
         modalInstance.result.then(function (status) {
            $modalInstance.close();
            $scope.status = status;
         }, function (status) {
            if (status != 'escape key press' && status != 'backdrop click') {
               $scope.status = status;
            } else {
               $modalStack.dismissAll();
            }
         });
      } else {
         $scope.status = "Order Failed! Please make sure all fields are valid";
      }
   };

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.close();
   };

}]);

//Porfolio controller
riskControllers.controller('PortfolioController', ['$scope', '$http', '$timeout', '$q', '$templateCache', 'UserFactory', 'OrderFactory', 'data', function($scope, $http, $timeout, $q, $templateCache, UserFactory, OrderFactory, data) {
   if (data.success) {
      $scope.loggedIn = true;
      $scope.user = data.response;
      $scope.codes = $scope.user.codes;
      $scope.holdings = $scope.user.holdings;
      $scope.transHistory = $scope.user.history;
   } else {
      $scope.loggedIn = false;
   }

   // Used on the portfolio page to determine whether there are any current
   // holdings or pending orders to display
   $scope.Object = Object;
   $scope.objectSize = function(object) {
      if (object) {
         return Object.keys(object).length;
      } else {
         return 0;
      }
   }
   // Gets the information from the user to populate the portfolio
   $scope.getPortfolio = function() {
      var deferred = $q.defer();
      UserFactory.get({'cash':true, 'share':true, 'pending':true, 'holdings':true, 'codes':true, 'history':true}, function(response){
         $scope.loggedIn = true;
         $scope.user = response;
         $scope.codes = $scope.user.codes;
         $scope.holdings = $scope.user.holdings;
         $scope.transHistory = $scope.user.history;
         deferred.resolve();
      }, function(response) {
         $scope.status = "Failed to view portfolio!"
         $scope.loggedIn = false;
         deferred.reject();
      });
      return deferred.promise
   };

   var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth()+1; //January is 0
   var yyyy = today.getFullYear();
   if(dd<10) dd='0'+dd;
   if(mm<10) mm='0'+mm;

   $scope.tday = yyyy + "-" + mm + "-" + dd; //today's date to a form of "2015-01-01"
   $scope.bday = $scope.birthday; //in a form "2015-01-01"
   $scope.firstShare = $scope.tday; //new Date($scope.bday);
   $scope.quant = {};

   $scope.fetch = function() {
      $scope.processing = "Processing ...";
      $scope.urls = [];
      var n = 0;
      angular.forEach($scope.codes, function(date, code) {
         if (date < $scope.firstShare) {
            $scope.firstShare = date;
         }
         if (date.length > 10) {
            $scope.start = date.substring(0,10);
            $scope.end = date.substring(11,20);
         } else {
            $scope.start = date;
            $scope.end = $scope.tday;
         }
         //fetches historical share prices until yesterday
         $scope.urls[n] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20" +
               "from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22" +
               code + "%22%20and%20startDate%20%3D%20%22" +
               $scope.start + "%22%20and%20endDate%20%3D%20%22" +
               $scope.end + "%22&format=json&diagnostics=true&env=store%3A%2F%2F" +
               "datatables.org%2Falltableswithkeys&callback=";
         n += 1;
      });
      var completedRequest = 0;
      $scope.prices = [[]];
      $scope.pricesLabels = [];

      for (var i = 0; i < $scope.urls.length; i++) {
         $http({method: 'GET', url: $scope.urls[i], cache: $templateCache}).then(function(response) {
            if (response.data && response.data.query && response.data.query.results && response.data.query.results.quote) {
               $scope.historicalStatus = response.status;
               $scope.historicalDataQuote = response.data.query.results.quote;
               for (var j = 0; j < $scope.historicalDataQuote.length; j += 1) {

                  $scope.quant = 0;
                  for (var time in $scope.transHistory) {
                     //do not want transactions after the Date
                     if (time.substring(0,10) > $scope.historicalDataQuote[j].Date) {
                        break;
                     }
                     $scope.transaction = $scope.transHistory[time];
                     if ($scope.transaction.type == 'buy' && $scope.transaction.stock == $scope.historicalDataQuote[j].Symbol) {
                        $scope.quant += $scope.transaction.quantity;
                     } else if ($scope.transaction.type == 'sell' && $scope.transaction.stock == $scope.historicalDataQuote[j].Symbol) {
                        $scope.quant -= $scope.transaction.quantity;
                     }

                     //when the date is not in the labels
                     if ($scope.pricesLabels.indexOf($scope.historicalDataQuote[j].Date) < 0) {
                        $scope.pricesLabels.unshift($scope.historicalDataQuote[j].Date);
                        //multiply by stock quantity on the date
                        $scope.holdingPrice = parseFloat($scope.historicalDataQuote[j].Close) * $scope.quant;
                        $scope.prices[0].unshift(parseFloat($scope.holdingPrice));
                     } else {
                        var index = $scope.pricesLabels.indexOf($scope.historicalDataQuote[j].Date);
                        $scope.holdingPrice = parseFloat($scope.historicalDataQuote[j].Close) * $scope.quant;
                        $scope.prices[0][index] += parseFloat($scope.holdingPrice);
                     }
                  }
               }
               completedRequest++;
               //when all the url data have been gathered.
               if (completedRequest == $scope.urls.length) {
                  if ($scope.bday < $scope.firstShare) {
                     $scope.birthdate = new Date($scope.bday);
                     $scope.shareDate = new Date($scope.firstShare);
                     while ($scope.birthdate < $scope.shareDate) {
                        $scope.shareDate.setDate($scope.shareDate.getDate() - 1);
                        //when it's not weekend
                        if ($scope.shareDate.getDay%6 != 0) {
                           var day = $scope.shareDate.getDate();
                           var month = $scope.shareDate.getMonth()+1; //January is 0!
                           var year = $scope.shareDate.getFullYear();
                           if(day<10) day='0'+day;
                           if(month<10) month='0'+month;
                           $scope.changedDate = year + "-" + month + "-" + day;
                           $scope.pricesLabels.unshift($scope.changedDate);
                           $scope.prices[0].unshift(parseFloat(0));
                        }
                     }
                  }
                  //axis label: weekly
                  for (var i = 0; i < $scope.pricesLabels.length; i++) {
                     if (i % 5) {
                        $scope.pricesLabels[i] = "";
                     }
                  }
               }
            } else {
               // TODO: HANDLE ERROR
            }
         }, function(response) {

         });
      }
      $scope.pricesSeries = [];
      $scope.pricesSeries = ['Prices'];
      $scope.options = {
         bezierCurve : false,
         pointDot : false,
         pointHitDetectionRadius : 1,
         showTooltips: false,
         animation: false
      };
      $scope.processing = "";
   };

   var verified = $scope.getPortfolio();
   verified.then(function(resolve) {
      $scope.fetch();
   });

   // Cancels the order specified by orderKey
   $scope.cancelOrder = function(orderKey) {
      OrderFactory.delete({'key': orderKey}, function(response) {
         // Successfully deleted. Refresh information on page
         $timeout(function() {
            $scope.getPortfolio();
         }, 0);
         $scope.status = "Order deleted"
      }, function(response) {
         $scope.status = "Failed to delete order"
      });
   };
}]);

// Welcome controller
riskControllers.controller('WelcomeController', ['$scope', '$http', 'UserFactory', function($scope, $http, UserFactory) {
   $scope.navModalsLR = ['Login', 'Register'];
   UserFactory.get({'email':true, 'nickname':true}, function(response) {
      $scope.email = response.email;
      $scope.nickname = response.nickname;
      $scope.loggedIn = true;
   }, function(error) {
      $scope.loggedIn = false;
   });
}]);


// Login controller
riskControllers.controller('LoginController', ['$scope', '$http', '$window', 'UserFactory', 'AccountFactory', '$modalInstance', function($scope, $http, $window, UserFactory, AccountFactory, $modalInstance) {
   // Initialise user
   $scope.user = {
      email: "",
      password: ""
   };

   UserFactory.get({'email':true, 'nickname':true}, function(response) {
      $scope.email = response.email;
      $scope.nickname = response.nickname;
      $scope.loggedIn = true;
   }, function(response) {
      $scope.loggedIn = false;
   });

   $scope.validateUser = function() {
      // TODO Think about how to do this without sending pass in clear
      AccountFactory.save({'login':true, 'request': angular.toJson($scope.user)}, function(response) {
         $modalInstance.close();
      }, function() {
         $scope.message = "Incorrect username or password";
      });
   }

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.dismiss();
   };
}]);

// Logout confirmation controller
riskControllers.controller('LogoutController', ['$scope','$modalInstance', function($scope, $modalInstance) {
   $scope.cancel = function() {
      $modalInstance.dismiss();
   }
}]);

riskControllers.controller('SignupController', ['$scope', '$http', 'AccountFactory', '$modalInstance', function($scope, $http, AccountFactory, $modalInstance) {
   $scope.addUser = function() {
      if($scope.user.password != $scope.user.password_again) {
         $scope.message = "Please ensure passwords match";
      } else {
         AccountFactory.save({'signup':true, 'request': angular.toJson($scope.user)}, function(response) {
            $modalInstance.close();
         }, function(){
            $scope.message = "A user with this email already exists!";
         });
      }
   }

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.dismiss();
   };
}]);

riskControllers.controller('ResetController', ['$scope', '$http', '$modalInstance', 'UserFactory', 'AccountFactory', function($scope, $http, $modalInstance, UserFactory, AccountFactory) {
   // Initialise user
   $scope.user = {
      email: "",
      password: ""
   };

   UserFactory.get({'email':true, 'nickname':true}, function(response){
      $scope.email = response.email;
      $scope.nickname = response.nickname;
      $scope.loggedIn = true;
   }, function(response) {
      $scope.loggedIn = false;
   });

   $scope.changeLogin = function() {
      var checkLogin = {
         'email'   : $scope.email,
         'password': $scope.pass.new1,
         'old_pass': $scope.pass.old
      };

      if ($scope.pass.new1 != $scope.pass.new2) {
         $scope.message = "Please ensure new passwords match";
      } else if ($scope.pass.new1 && $scope.email && $scope.pass.old) {
         AccountFactory.save({'reset':true, 'request': angular.toJson(checkLogin)}, function(response) {
            $scope.message = "Password changed successfully!";
         }, function(){
            $scope.message = "Something went wrong in password update - check old password";
         });
      } else {
         $scope.message = "Something went wrong in password update - check old password";
      }
   }

   // Close the modal
   $scope.cancel = function () {
      $modalInstance.dismiss();
   };
}]);

// Handles the confirmation window modal
riskControllers.controller('OrderController', ['$scope', '$log', '$modalInstance', '$q', 'UserFactory', 'OrderFactory', '$modalStack', function ($scope, $log, $modalInstance, $q, UserFactory, OrderFactory, $modalStack) {
   // Close the modal
   $scope.cancel = function () {
      $modalStack.dismissAll();
   };

   // A function to validate the order form. The order form must be
   // valid for an order to progress. Provides a promise to ensure
   // the request is synchronous
   $scope.validate = function(){
      var valid = false;
      var deferred = $q.defer();

      // Get user information
      UserFactory.get({'cash':true, 'sellable_shares':true}, function(response){
         $scope.userCash = response.cash;
         $scope.sellableShares = response.sellable_shares;

         // Further validation (modal handles formatting validation
         if ($scope.order.type == "buy"
            && $scope.userCash >= $scope.order.price * $scope.order.quantity + 20) {
            deferred.resolve("Order Successful!");
         } else if ($scope.order.type == "buy") {
            deferred.reject("You do not have enough money to purchase that amount of shares!");
         } else if ($scope.order.type == "sell" &&
            parseFloat($scope.sellableShares[$scope.order.stock]) >= $scope.order.quantity) {
            deferred.resolve("Order Successful!");
         } else if ($scope.order.type == "sell") {
            deferred.reject("You are trying to sell more shares than you own");
         }
      }, function() {
         deferred.reject("Order Failed! Please try again");
      });
      return deferred.promise;
   }


   // Handle when the submit button is pressed to send an order request
   // Changes status according to success or failure
   $scope.addOrder = function(){
      $scope.submitted = true;

      // The order request is only processed if valid.
      var validated = $scope.validate();
      validated.then(function(resolve) {

         // Process the order for the corresponding user account
         UserFactory.get({'email':true}, function(response){
            $scope.order.email = response.email;

            // Hack to keep price on order_confirmation the same (TODO fix)
            $scope.tempPrice = $scope.order.price;

            // Don't execute order immediately. Every 5 minutes orders will be executed by cron
            $scope.order.executed = false;

            // Send the order and give a message if successful
            OrderFactory.save({'order':angular.toJson($scope.order)}, function(response) {
               $scope.status = resolve;
               $modalInstance.close($scope.status);
            }, function(){
               $scope.status = "Order Failed! Please try again";
               $modalInstance.close($scope.status);
            });

            // Hack to keep price on order_confirmation the same (TODO fix)
            $scope.order.price = $scope.tempPrice;

         }, function(response) {
            $scope.status = "Order Failed! Please try again";
            $modalStack.dismissAll($scope.status);
         });
      }, function(reject){
         $scope.status = reject;
         $modalStack.dismissAll($scope.status);
      });
   };
}]);

// Historical transactions controller
riskControllers.controller('HistoryController', ['$scope', 'data', function($scope, data) {
   if (data.success) {
      $scope.loggedIn = true;
      $scope.transactions = data.response.history;
   } else {
      $scope.loggedIn = false;
   }

   // Used on the history page to determine whether there are any transactions
   // to display
   $scope.Object = Object;
   $scope.objectSize = function(object) {
      if (object) {
         return Object.keys(object).length;
      } else {
         return 0;
      }
   }
}]);

// Leaderboard controller
riskControllers.controller('LeaderboardController', ['$scope', '$http', 'userData', function($scope, $http, userData) {
   // 'data' contains user email.
   if (userData.success) {
      $scope.loggedIn = true;
      $scope.user = userData.response;
      $http.get('/leaderboard').success(function(response) {
         $scope.players = response;
         $scope.list = [];
         for (var email in $scope.players) {
            $scope.list.push([ email, $scope.players[email][0], $scope.players[email][1], $scope.players[email][2], $scope.players[email][3], $scope.players[email][4] ]);
         };
         $scope.list.sort(function(a, b) {return b[5] - a[5]});
      }, function(response) {
         // TODO: handle error.
      });
   } else {
      $scope.loggedIn = false;
   }
}]);


// Handles modals for login and signup
riskControllers.controller('NavController', ['$scope', '$modal', '$route', '$location', function($scope, $modal, $route, $location) {
   $scope.navModals = ['Account', 'Logout'];
   $scope.navPills = [{
         label: 'Home',
         class: '',
         href: '/#/'
      },
      {
         label: 'Trade',
         class: '',
         href: '/#/trade'
      },
      {
         label: 'Portfolio',
         class: '',
         href: '/#/portfolio'
      },
      {
         label: 'History',
         class: '',
         href: '/#/history'
      },
      {
         label: 'Leaderboard',
         class: '',
         href: '/#/leaderboard'
      }
   ];

   // Make the pill corresponding to the current index active
   $scope.updateNav = function(index) {
      for (var i = 0; i < $scope.navPills.length; i++) {
         if (i == parseFloat(index)) {
            $scope.navPills[i].class = 'active';
         } else {
            $scope.navPills[i].class = '';
         }
      }
   }

   // Open the corresponding modals to handle user accounts
   $scope.openModal = function(label) {
      if (label == 'Login') {
         var templateUrl = '/app/partials/login.html';
         var controller = 'LoginController';
      } else if (label == 'Register') {
         var templateUrl = '/app/partials/signup.html';
         var controller = 'SignupController';
      } else if (label == 'Account') {
         var templateUrl = '/app/partials/reset.html';
         var controller = 'ResetController';
      } else if (label == 'Logout') {
         var templateUrl = '/app/partials/logout.html';
         var controller = 'LogoutController';
      }
      var modalInstance = $modal.open({
         animation: true,
         templateUrl: templateUrl,
         controller: controller
      });

      modalInstance.result.then(function () {
         $route.reload();
      });
   }

   $scope.locationPath = function (newLocation) {
      return $location.path(newLocation);
   };
}]);

// Login Modal