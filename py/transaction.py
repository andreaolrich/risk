""" Transaction Model Class

    This module contains the model for transactions (i.e. buying and selling
    stocks) and all related functions.

"""

from google.appengine.ext import ndb

class Transaction(ndb.Model):
   type = ndb.StringProperty(required=True, choices=['buy', 'sell'])
   subtype = ndb.StringProperty(required=True, choices=['market', 'limit', 'stop'])
   stock = ndb.StringProperty(required=True)
   price = ndb.FloatProperty(required=True)
   quantity = ndb.IntegerProperty(required=True)
   timestamp = ndb.DateTimeProperty(auto_now_add=True, required=False)
   executed = ndb.BooleanProperty(required=True)
   fee = ndb.FloatProperty(required=True)

   @classmethod
   def new(cls, data):
      """ Creates a new instance of the class and adds it to the datastore.
          Assumes data passed is valid.

      Keyword arguments:
      data -- a dictionary containing the model instance data

      """
      transaction = cls(type=data['type'],subtype=data['subtype'],stock=data['stock'],price=data['price'],quantity=data['quantity'],executed=data['executed'],fee=data['fee'])

      transaction.put()
      return transaction

   @classmethod
   def is_valid(cls, data):
      """ Checks if data in the dictionary is valid according to the model schema.

      Keyword arguments:
      data -- a dictionary containing data for this model

      """
      if ((data['type'] == 'buy' or data['type'] == 'sell')
         and (data['subtype'] == 'market' or data['subtype'] == 'limit' or data['subtype'] == 'stop')
         and data['price'] >= 0
         and data['quantity'] > 0
         and data['executed'] == True or data['executed'] == False):
         return True
      else:
         return False

   @classmethod
   def delete(cls, key):
      """ Deletes the transaction from the datastore

        Keyword arguments:
        key -- the key corresponding to the transaction

        Throws: AttributeError if key is not valid

      """
      try:
         transaction = key.get()
         transaction.key.delete()
      except AttributeError:
         raise AttributeError("Invalid key")


