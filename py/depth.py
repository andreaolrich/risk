""" Market Depth Model Class

    This module represents the model of the market depth
    associated with a particular stock
"""

from google.appengine.ext import ndb

import random
import numpy
import json

class Depth(ndb.Model):
   stock = ndb.StringProperty(required=True)
   bids = ndb.JsonProperty()
   asks = ndb.JsonProperty()
   max_bid = ndb.FloatProperty(required=True)
   min_ask = ndb.FloatProperty(required=True)


   @classmethod
   def new (cls, data):
      # The number of prices to generate order depths for
      NUM_PRICES = 10

      bids = json.dumps(cls.generate_depth('bid', data['max_bid'], data['avg_volume'], NUM_PRICES))
      asks = json.dumps(cls.generate_depth('ask', data['min_ask'], data['avg_volume'], NUM_PRICES))
      depth = cls(stock=data['stock'], bids=bids, asks=asks, max_bid=data['max_bid'], min_ask=data['min_ask'])
      depth.put()
      return depth

   @classmethod
   def generate_depth(cls, type, limit, avg_volume, num_prices):
      # The initial probability whereby an order price is skipped
      INIT_SKIP_PROB = 0.20

      # The amount to increment the probability of skipping an order price
      SKIP_PROB_INC = 0.02

      # The amount the order price changes
      PRICE_CHANGE = .01

      # Dict of prices
      depth = {}

      # Start off at the current price
      current_price = float(limit)

      # Based off observations, 0.001% seems to be mean volume for any one order for stocks over
      # $30, but for stocks immediately under $30 it seems to be 0.01%
      # NOTE: Check further and restrict to ASX200
      mean = 0.0001 * float(avg_volume)
      if (limit < 30):
         mean = 0.001 * float(avg_volume)

      # The initial probability
      p = INIT_SKIP_PROB

      # Algorithm: We'll generate it for 10 prices. We want to move down in .01
      # increments with perhaps a 20% chance of skipping an increment. This chance
      # increases as we get further away from the bid
      i = 0
      while (i < num_prices):
         # Only add the order to the depth with a probability of 1 - p
         # unless the order is at the bid limit (in which case that necessarily)
         # means an order should be there
         rand = random.random()

         if (rand > p or current_price == float(limit)):

            # Generate the volume for this price based off average daily volume.
            # Orders are very variable so SD is extremely large
            volume = numpy.random.normal(mean, mean*4)
            if (volume < 0): volume = volume * -1

            # Continually increase the probability of skipping an order
            # once we are past the first num_prices/2.
            if (i > (num_prices / 2)):
               p += SKIP_PROB_INC

            # Add the volume, num orders to the depth dict
            depth[round(current_price, 2)] = [round(volume, 0)]

            # Increment the counter
            i += 1

         # Decrement the price by PRICE_CHANGE
         if (type == 'bid'):
            current_price -= PRICE_CHANGE
         elif (type == 'ask'):
            current_price += PRICE_CHANGE

      return depth

   @classmethod
   def get(cls, data):
      depthQuery = Depth.query(Depth.stock == data['stock'])
      if (depthQuery.count() == 0):
         depth = Depth.new(data)
      else:
         depth = depthQuery.get()
         depth.update(data['max_bid'], data['min_ask'], data['avg_volume'])
         depth.put()
      return depth

   """
      Updates the depth to fit in with the current bid and ask prices
   """
   def update(self, new_bid, new_ask, avg_volume):
      if (new_bid < self.max_bid):
         new_bid_list = self.update_int_depth('bid', new_bid, avg_volume)
      elif (new_bid > self.max_bid):
         new_bid_list = self.update_ext_depth('bid', new_bid, avg_volume)
      else:
         new_bid_list = json.loads(self.bids)
         new_bid = self.max_bid

      if (new_ask > self.min_ask):
         new_ask_list = self.update_int_depth('ask', new_ask, avg_volume)
      elif (new_ask < self.min_ask):
         new_ask_list = self.update_ext_depth('ask', new_ask, avg_volume)
      else:
         new_ask_list = json.loads(self.asks)
         new_ask = self.min_ask


      self.bids = json.dumps(new_bid_list)
      self.asks = json.dumps(new_ask_list)
      self.max_bid = new_bid
      self.min_ask = new_ask

   """
      Updates depth where there has been an internal overlap
      (i.e. new bid is less than old bid, or new ask is greater than old ask)

      Keyword arguments:
      type -- 'bid' or 'ask'
      limit -- the new limit for the price (the new bid or new ask)
      avg_volume -- the average volume for the share
   """
   def update_int_depth(self, type, limit, avg_volume):
       # The amount the order price changes
      PRICE_CHANGE = .01

      new_depth = {}
      if (type == 'bid'):
         old_depth = json.loads(self.bids)
      elif (type == 'ask'):
         old_depth = json.loads(self.asks)

      new_limit = limit
      for price in old_depth.iterkeys():
         # Keep all the elements in the old list that are in the
         # constraints of the new limit
         if ((type == 'bid' and float(price) <= limit) or
            (type == 'ask' and float(price) >= limit)):
            new_depth[price] = old_depth[price]

         # Establish the minimum (for bid) or maximum (for ask)
         # element in the new list and generate the remainder of elements
         # from that
         if (type == 'bid' and float(price) <= new_limit):
            new_limit = float(price) - PRICE_CHANGE
         elif (type == 'ask' and float(price) >= new_limit):
            new_limit = float(price) + PRICE_CHANGE


      # Generate the remaining elements
      new_depth.update(Depth.generate_depth(type,
         new_limit, avg_volume, 10 - len(new_depth)))

      return new_depth


   """
      Updates depth where there has been an external overlap
      (i.e new bid is greater than old bid, or new ask is less than old ask)

      Keyword arguments:
         type -- 'bid' or 'ask'
         limit -- the new limit for the price (the new bid or new ask)
         avg_volume -- the average volume for the share
   """
   def update_ext_depth(self, type, limit, avg_volume):
      if (type == 'bid'):
         old_depth = json.loads(self.bids)
      elif (type == 'ask'):
         old_depth = json.loads(self.asks)

      # Generate an initial list of 10, combine it with the previous list
      # and then select the top 10
      new_depth = Depth.generate_depth(type, limit, avg_volume, 10)
      new_depth.update(old_depth)
      i = 0
      for key in sorted(new_depth.iterkeys()):
         if (i >= 10):
            del new_depth[key]
         i += 1

      return new_depth







